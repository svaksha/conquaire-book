%% This is file `elsarticle-template-1-num.tex',
% * <swachsmu@techfak.uni-bielefeld.de> 2018-07-24T13:44:52.097Z:
%
% ^.
%%
%% Copyright 2009 Elsevier Ltd
%%
%% This file is part of the 'Elsarticle Bundle'.
%% ---------------------------------------------
%%
%% It may be distributed under the conditions of the LaTeX Project Public
%% License, either version 1.2 of this license or (at your option) any
%% later version.  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.2 or later is part of all distributions of LaTeX
%% version 1999/12/01 or later.
%%
%% Template article for Elsevier's document class `elsarticle'
%% with numbered style bibliographic references
%%
%% $Id: elsarticle-template-1-num.tex 149 2009-10-08 05:01:15Z rishi $
%% $URL: http://lenova.river-valley.com/svn/elsbst/trunk/elsarticle-template-1-num.tex $
%%
\documentclass[preprint,12pt]{elsarticle}

%% Use the option review to obtain double line spacing
%% \documentclass[preprint,review,12pt]{elsarticle}

%% Use the options 1p,twocolumn; 3p; 3p,twocolumn; 5p; or 5p,twocolumn
%% for a journal layout:
%% \documentclass[final,1p,times]{elsarticle}
%% \documentclass[final,1p,times,twocolumn]{elsarticle}
%% \documentclass[final,3p,times]{elsarticle}
%% \documentclass[final,3p,times,twocolumn]{elsarticle}
%% \documentclass[final,5p,times]{elsarticle}
%% \documentclass[final,5p,times,twocolumn]{elsarticle}

%% The graphicx package provides the includegraphics command.
\usepackage{graphicx}
%% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb}
%% The amsthm package provides extended theorem environments
%% \usepackage{amsthm}

%% The lineno packages adds line numbers. Start line numbering with
%% \begin{linenumbers}, end it with \end{linenumbers}. Or switch it on
%% for the whole article with \linenumbers after \end{frontmatter}.
\usepackage{lineno}

%% natbib.sty is loaded by default. However, natbib options can be
%% provided with \biboptions{...} command. Following options are
%% valid:

%%   round  -  round parentheses are used (default)
%%   square -  square brackets are used   [option]
%%   curly  -  curly braces are used      {option}
%%   angle  -  angle brackets are used    <option>
%%   semicolon  -  multiple citations separated by semi-colon
%%   colon  - same as semicolon, an earlier confusion
%%   comma  -  separated by comma
%%   numbers-  selects numerical citations
%%   super  -  numerical citations as superscripts
%%   sort   -  sorts multiple citations according to order in ref. list
%%   sort&compress   -  like sort, but also compresses numerical citations
%%   compress - compresses without sorting
%%
%% \biboptions{comma,round}

% \biboptions{}

\usepackage{hyperref}  % for links, etc.
\usepackage{wrapfig}
\usepackage{subfigure}
%\usepackage{graphicx}
%\usepackage{graphics}

\journal{Proc. of Workshop on Reproducibility}

\begin{document}

\begin{frontmatter}

%% Title, authors and addresses

\title{Reproducibility in Human-Robot Interaction Research: A Case Study}

%% use the tnoteref command within \title for footnotes;
%% use the tnotetext command for the associated footnote;
%% use the fnref command within \author or \address for footnotes;
%% use the fntext command for the associated footnote;
%% use the corref command within \author for corresponding author footnotes;
%% use the cortext command for the associated footnote;
%% use the ead command for the email address,
%% and the form \ead[url] for the home page:
%%
%% \title{Title\tnoteref{label1}}
%% \tnotetext[label1]{}
%% \author{Name\corref{cor1}\fnref{label2}}
%% \ead{email address}
%% \ead[url]{home page}
%% \fntext[label2]{}
%% \cortext[cor1]{}
%% \address{Address\fnref{label3}}
%% \fntext[label3]{}


%% use optional labels to link authors explicitly to addresses:
%% \author[label1,label2]{<author name>}
%% \address[label1]{<address>}
%% \address[label2]{<address>}

\author[unibi]{Florian Lier}
\author[unibi]{Phillip L{\"u}cking}
\author[unibi]{Sebastian Meyer zu Borgsen}
\author[unibi]{Sven Wachsmuth}
\author[unibi]{Jasmin Bernotat}
\author[unibi]{Friderieke Eyssel}
\author[indiana]{Robert Goldstone}
\author[indiana]{Selma \v{S}abanovi\'{c}}
\address[unibi]{CITEC, Bielefeld University, Germany}
\address[indiana]{Indiana University Bloomington}

\begin{abstract}
%% Text of abstract
Studies in human-robot interaction (HRI) typically involve computational artifacts, 
i.e. the robotic system, as the subject of investigation. 
Thus, the reproducibility of any result in HRI studies directly relates to the reproducibility of this computational artifact in the first place. This has certain consequences for appropriate workflows that will be discussed in this contribution.
We argue for a higher awareness, improved standards, and further automation 
of tool chains used to conduct robotic experiments. We identify this as a research topic in its own 
right, especially in cases where robotic systems are used in interdisciplinary research. 
This inherently includes that technically complex robotic experiments should also be reproducible 
by scientists with a \emph{non-technical} background. We analyze and discuss a dedicated study by 
the CITEC Central Lab Facilties and an international team demonstrating 
that it is possible to replicate a reasonably complex HRI experiment in two different laboratories across the globe by a research assistant with no experience in robotics at all. 

\end{abstract}

\begin{keyword}
Human-Robot Interaction \sep Reproducibility \sep Robotic experiments
%% keywords here, in the form: keyword \sep keyword

%% MSC codes here, in the form: \MSC code \sep code
%% or \MSC[2008] code \sep code (2000 is the default)

\end{keyword}

\end{frontmatter}

%%
%% Start line numbering here if you want
%%
\linenumbers

%% main text
\section{Introduction}
\label{sec:introduction}
% 1-2 pages
% describe general research agenda of the research group
% describe the research question
% describe the main result from the publication reproduced within conquaire

The Central Lab Facilities (CLF) group of the Excellence Cluster Cognitive Interaction Technology (CITEC) at Bielefeld University aims to develop and improve technology, workflows, and tool chains for building as well as experimenting with
interactive intelligent systems \cite{lier_citk_2016,lier2014cognitive,fsmt,morseSIMPAR2014,Borgsen2018-MRHRI}.  
An important application and research field is human-robot interaction (HRI) which requires 
sophisticated robotic research platforms that include many software and hardware challenges besides the core
areas of perception, behavior generation, and interaction design.
%
Thus, research in HRI is a highly interdisciplinary endeavor.   It aims to model the physical as well as mental dynamics between a human and a robot in a communicative or cooperative situation. It draws many concepts and ideas from human-human interaction in order to make the human-robot interface as smooth and intuitive as possible. Dealing with physically embodied agents, this includes many engineering issues towards flexible and save movements, many issues from machine
perception, e.g. recognizing the interaction partner, many issues from artificial intelligence
towards an interpretable and goal-oriented behavior of the robot, as well as many issues explored
by the social sciences (psychology, linguistics, cognitive science, etc.) in order to understand associations, attributions, and expectations that humans have in their interaction with a robot.
Last but not least, any experiment with an autonomous robot includes many system engineering challenges including significant complexity issues on the software side which are frequently underestimated. 
%
Although, there has been considerable progress in robot technology including available robotic 
standard platforms (e.g. iCub, Softbank's Nao and Pepper, Toyota's HSR), software frameworks \cite{quigley2009ros,orocos-2001,metta2006yarp,naoqi}, and benchmarking activities
\cite{Wachsmuth2015RoboCupHome,Lima-EuropeanRoboticsLeague-2016,2013benchmarking,QuispeAmorChristensen2018-benchmark}, the theoretical and practical foundations for experimental replicability of experiments in robotics is still in its infancy \cite{bonline2017}. 
In this regard, Bonsignorio et al., e.g., states ``even determining the information required to enable replication of results has been subject of extensive discussion'' \cite{bonline2017}.
%

%
% Relation to CONQUAIRE project (here "we" means the CONQUAIRE project)
%
In this contribution, we argue for a higher awareness, improved standards, and further automation of tool chains used to conduct robotic experiments. We identify this as a research topic in its own right, especially in cases where robotic systems are used in interdisciplinary research. This inherently includes that technically complex robotic experiments should also be reproducible by scientists with a \emph{non-technical} background.
%
This takes a look slightly beyond the core goals of the CONQUAIRE project \cite{Conquair16:online} of the pure analytic reproducibility. However, we argue that in the case of human-robot interaction studies, the replication of the technical system
is essential to understand the data of the experiment. 
%
The other CONQUAIRE studies mostly deal with computational workflows and tools that are applied to
datasets \emph{after} these have been recorded in an experiment. Because most studies in, e.g., the natural sciences deal with 'natural' phenomena -- i.e. they are not produced by an artificial artifact --
the dataset can be interpreted with regard to this phenomenon at any place in the world. 
This is not the case for experiments including robots.
The dataset can only be interpreted with regard to the artificial artifacts used in the experiment.
As a consequence, the reproducibility of an experiment and the validity of the data must include the possibility to reproduce also the robotic system and its behavior in the study.
%

In the following, we report our experiences and lessons learned in analysing a replication study by the Central Lab Facilities
conducting a human-robot interaction (HRI) experiment in Bielefeld and at a partner site of the DFG Excellence Cluster CITEC 
within the DAAD Thematic Network Interactive Intelligent Systems. The study investigates an extended version of Stenzel et al's ``Joint Simon effects for non-human co-actors''~\cite{stenzel2012humanoid}, in two labs in different institutions and continents.
%
In psychology, the Joint Simon effect is used to investigate to what extend people mentally
represent their own and other agent's actions in a joint task. 
This leads to delayed decision effects when a human is prompted with stimuli that are spatially incompatible
with the roles in a team.
%
The effect disappears when people
think that they interact with a non-biological, technical artifact.
Thus, it is an open question to which degree humanoid robots are perceived as social agents or team mates and
if this can be shown using the Joint Simon effect (see~Sec.~\ref{sec:JSE} for more details).

To this end, the CLF researchers applied a novel software tool chain and methodology that implements state-of-the-art techniques with the objective of facilitating reproducibility in robotics research. The experiment was designed in cooperation between Bielefeld University$^{1}$ and Indiana University Bloomington$^{2}$ by a team of interdisciplinary scientists --- originating from psychology \& brain sciences, informatics and robotics. The team initially conducted the study in Bielefeld before a replication attempt in Indiana was made. In this context, they specifically chose the following  constraints in order to impose the same restrictions and obstacles encountered in ``regular'' replication attempts.
\begin{enumerate} 
\item The experiment must be replicated by a staff member who is \emph{not} part of the research project.
\item The only starting point for replication is an online manual explaining our approach and
the therein referenced literature.
\item Assistance from Bielefeld is only provided in otherwise irresolvable situations.
\end{enumerate}
A replication of this experiment at different sites is an interesting case study with regard to
two different aspects. On the one hand, it is interesting to see, if there are cultural factors that
influence the results. On the other hand, the setup includes a behavioral study with a robotic platform (the NAO robot) which is programmed to physically press a button where timing matters.
Thus, from the perspective of reproducibility and the lessons learned for the CONQUAIRE project, there are the following research questions:
\textbf{(H1)} Has the tool chain and methodology been suitable to represent all aspects required for successful replication? 
\textbf{(H2)}  What can we learn about reproducibility in general with respect to unexpected technical obstacles or situations one did not anticipate? 
\textbf{(H3)}  Could the second study cross-validate the results obtained in the original Bielefeld study?

\section{Experimental Settings and Methods}
% 2-3 pages
% which experimental settings were used in the reproduced paper
% which methods did you (partner) apply to analyze the data
% which were the main results of the paper - include pictures

The following part of this contribution will cover the replication approach and the lessons learned. 
Important parts of the study and tool chain was published in \cite{Lier2017CanWR,Luecking2018-GDD}. A final
evaluation of the second study is still on-going work.
First, we will shortly introduce to the theoretical background of the experiment, then, present the procedure and methods, and finally discuss our findings. 

\subsection{The JSE Experiment}
\label{sec:JSE}

The study chose to reproduce a variant~\cite{dolk2014joint} of a well-documented psychological effect, the Joint Simon Effect (JSE)~\cite{stenzel2012humanoid}. The JSE describes a difference in reaction time depending on identity (\textit{compatibility}) or disparity (\textit{incompatibility}) of a stimulus' and the co-actors' spatial position in relation to the participant during a shared go/no-go task. The team aimed at reproducing it with a robot as co-actor as in~\cite{dolk2014joint} and adopted the stimuli and procedure attributes. The original experiment was extended with a \textit{robot position} condition to additionally test the influence of the robot's spatial relation to the human subject.
While more detailed information about the JSE experiment can be found in~\cite{dolk2014joint}, we will briefly describe the experiment setup variant. Due to its wide distribution and availability, the team used the humanoid robot NAO as the participant's co-actor (Figure~\ref{fig:setup}). The robot kneels next to the test subject on a table or chair. The barycenter of the robot is approximately at elbow height of a sitting subject. 

The participant and the robot each have their own keyboard of identical type. The keyboards are directly adjacent and on the same level. During the experiment, stimuli, e.g. a square and a diamond, are displayed on a screen at randomized positions and in randomized order. Based on the initial assignment, either the robot, or the human have to press the space-bar key as soon as the assigned stimulus appears. The corresponding reaction times (RT) of the human co-actor are measured.

In Bielefeld, the team tested 47 subjects from the nearby campus (\textit{M} age = 24.61 years, \textit{SD} age = 4.01 years). Each run consisted of 512 trials with short breaks per 128 trials and took approximately 30 minutes. The findings were similar to those found by Stenzel et al., the experiment showed a significant main effect of compatibility when analyzing the response times (RT), \textit{F}(1,48) = 11.639, \textit{p} $<$ 0.001, partial $\eta^2$ = .43, indicating shorter RTs in compatible (423 ms) compared with incompatible trials (434 ms) which confirms the presence of an overall JSE. The team did not find a significant interaction between \textit{compatibility} and \textit{robot position}.

The data of the experiment was logged within the software tool jsPsych \cite{de2015jspsych} that controlled the prompting, triggered the execution of robot movements, and recorded the reactions of the human participants 
(execution protocoll of the experiment including timing events for prompting and robot, spatial configuration of prompts, etc.). The data is stored as comma-separated-value files which are preprocessed with documented shell commands and
python scripts. A deep analysis was performed by the SPSS\footnote{\url{https://www.ibm.com/de-de/products/spss-statistics}} 
or R\footnote{\url{https://www.r-project.org/}} tools. 

%the software controlling the robot (execution protocoll of the robot including the timing and movement of the robot) and

\begin{figure}[ht]
	\begin{minipage}[c]{0.22\linewidth}
	\centering
	\includegraphics[width=1\textwidth]{images/pepper_setup_table_small.jpg}
	\caption{The NAO JSE setup used in one of our Bielefeld setups (cf. Figure~\ref{fig:calib})}
	\label{fig:setup}
	\end{minipage}
	\hspace{0.35cm}
	\begin{minipage}[c]{0.22\linewidth}
	\centering
	\includegraphics[width=1\textwidth]{images/pepper_press_small.jpg}
	\caption{NAO keypress pose\vspace{3.5em}}
	\label{fig:press}
	\end{minipage}
	\hspace{0.35cm}
	\begin{minipage}[c]{0.50\linewidth}
	\centering
    \includegraphics[width=\textwidth]{images/calib_small.jpg}
    \caption{Screenshot of the robot calibration GUI}
    \label{fig:calib}
	\end{minipage}

\end{figure}

\subsection{Replication in Indiana}
\label{sec:approach}

In order to reproduce the experiment in Indiana, under consideration of the demands and requirements in the current literature and the issues presented in section~\ref{sec:introduction}, there are two core issues to be solved:
\begin{enumerate}
\item A systemic solution for deployment, configuration, and integration of all necessary software artifacts
\item A structured methodological ``how-to'' for setup and execution considering user groups and tools from other disciplines, here, psychology.
\end{enumerate}
This should not come as overhead for the replication of an experiment.  It is essential that the replication tool chain
is already in place and used, when the first experiment is developed and conducted. Thus, the replicability of
an experiment including software-intensive systems as core components is decided at its design time. 

\subsubsection{The replication tool chain}

In order to address the above issues the research team developed a software tool chain that has been explicitly 
designed to foster reproducibility of software intensive experiments in robotics --- the \emph{Cognitive Interaction Toolkit} (CITK)~\footnote{\url{https://toolkit.cit-ec.uni-bielefeld.de}}. More detailed technical information are explained in~\cite{lier_citk_2016} and~\cite{lier2014cognitive}. 
The requirement to support disciplinary tools to design and run experiments will be additionally covered by jsPsych~\cite{de2015jspsych}. 

\begin{figure}[h]
\begin{minipage}[c]{0.43\textwidth}
\subfigure[Overview of the toolchain]{
    \includegraphics[width=\textwidth]{images/ICRA_CITK_4_1.png}}
\end{minipage}
\hspace{0.15cm}
\subfigure[Screenshot of the CI server web front-end. Each row corresponds to a recipe that has been translated into a build job. Build jobs can be activated by selecting the most right icon (stopwatch). Execution of an experiment can also be triggered using this front-end.]{
    \includegraphics[width=0.55\textwidth]{images/jenkins_done_complete.png}
    \label{fig:jenkins}}
    \vspace{-0.3cm}
	\caption{Toolchain for the replication of robotics experiments: An experiment the
    	associated computational artifacts are documented in a browsable online catalog which is linked
        to the corresponding repositories. From here a researcher is instructed to automatically roll out the system distribution using a continuous integration server which is linked to the robot platform as well as the computer controlling the behavioral experiment using JsPsych.}
    \label{fig:CITK}
\end{figure}

At its core, the CITK provides a text- and template-based ``artifact-description'' repository in order to pool and aggregate all required artifacts of a robotics experiment (cf.~\ref{fig:CITK}). There are basically two natures of descriptions. The first is called recipe: it defines required system artifacts, e.g, software components, downloadable data sets, or system configuration files. Templates for new types of artifacts can be added on-the-fly by developers. With regard to pure software aspects, the existing set of templates contains macros for the most common build tools like autotools, maven, CMake, and ROS/catkin\footnote{\url{http://wiki.ros.org/catkin/conceptual_overview}}, enabling native builds of various kinds of software. These macros also help to remove redundancy and keep the recipes clean and well-structured --- this is \emph{not yet another} build tool. 

The second type is called distribution. A distribution is a composition of N arbitrary recipes and hence determines an entire system. Distributions, as well as recipes, mandatorily reference \emph{versions}, e.g., tags, branches, or commit hashes of an artifact, thus a distribution reflects a \emph{fixed} description of a system. 
Recipe and distribution files are publicly available in our GIT-repository~\footnote{\url{https://opensource.cit-ec.de/projects/citk}}.

Another core-component is a pre-packaged, i.e, download and run it, no configuration required, CI server. It is utilized to compile, deploy, and run entire software systems defined in distribution files. The server provides a web front-end that can be accessed via browser for ease of use. In order deploy and run a system, the CITK implements a generator-based approach. A so-called build-job-configurator tool automatically creates all required build-jobs (for every recipe in a distribution) on the server. A user merely selects the desired distribution file.

Moreover, it is also possible to connect a physical robot to the machine that runs the CI server in order to control/actuate it. Lastly, our approach also provides a framework to automatically bring up (stateful execution), stop, and introspect a robotics software system. Executing a system merely requires to select and activate a designated build-job in the web front-end. Data that is acquired/logged during each system run is also stored on the server and accessible via web browser. 

By utilizing this part of our structured CITK approach, the team could ensure technical reproducibility of all required artifacts and also repeatable experiment execution (re-activate the corresponding build-job) regarding the software side of an experiment. An exemplary CITK tool chain demonstration video can be watched here:~\url{https://vimeo.com/205541757}

With respect to experiment design and orchestration, the study additionally made use of a framework called jsPsych. jsPsych is a JavaScript library for creating behavioral experiments in a web browser. It provides a description of the experiment structure in the form of a time line. It handles which trial to run next and storing the obtained data. jsPsych uses plugins to define what to execute at each point on the time line. The functionality of jsPsych was extended in order to i) trigger an experiment run on the our CI server and b) execute experiment-specific behaviors of the NAO/Pepper (at the time of writing) robots, e.g, based on the current state of the time line in jsPsych. Detailed information about jsPsych can be found in~\cite{de2015jspsych}.

\subsubsection{The replication experiment}

Due to the fact that the entire software system was already modeled using the CITK for the Bielefeld study~\footnote{\url{http://www.webcitation.org/6xlwomECk}}, \textbf{no additional work, besides the translation from German to English, e.g, in the jsPsych time-line slides was required}. Hence, the software part including robot movement control interfaces, calibration procedures, and jsPsych experiment orchestration was already at hand. Since there was no prior knowledge about the (scientific) background of the staff member who would eventually replicate the experiment in Indiana, the team implemented a generic GUI-based application for all crucial technical steps with respect to the robot \emph{hardware}, e.g, the calibration procedures. 

Finally, a detailed instruction was compiled on a public GitHub page (final version~\footnote{\url{https://git.io/vAxml}}). This online manual included the following steps.
(1) Introduction, 
(2) Hardware Requirements and Prerequisites,
(3) Software Requirements and Prerequisites,
(4) Physical Experiment Setup,
(5) Subjects,
(6) Executing the Experiment,
(7) Results,
(8) Literature.
%
In summary, the manual included the following content. A brief introduction to the research topic and study goals, plus references to related literature. A specification of the required hardware, e.g, a NAO robot acquired within 2-3 years, a PC or laptop including CPU and RAM specifications --- even the size, resolution and refresh rate of the utilized screens. A specification of the operating system requirements, i.e., Ubuntu Xenial (16.04, 64 bit). An explanation of how to setup the physical experiment, such as height and position of the robot, position of the keyboards, monitors, etc. Furthermore, a brief explanation of the network setup. 

Moreover, the document included detailed instructions about the installation and usage of the CITK in order to deploy the software system, calibrate the robot, and run the experiment. The instructions also provided information about the subjects, the welcome and actual experiment procedure. Lastly, it was explained how to obtain and inspect the gathered data.

So far, the documentation included detailed information with respect to technical (soft- and hardware), as well as methodological/procedural aspects, to reproduce the study as it was conducted in Bielefeld. The team also established a communication channel via instant messaging using Slack. The channel was intended to provide ``emergency support'' --- but only in case of an otherwise irresolvable situation. Hence, the chat history could also be exploited for post-experiment analysis, if required.

\section{Computational Reproducibility: Results \& Lessons Learned}
% 2 pages
% describe the specific result(s) that has been reproduced
% describe pipeline used to reproduce the results
% describe formats and software tools
% highlight the technical challenges and issues discovered
% Describe the structure of the data in the gitlab repo, if any
% Comment computational reproduction success levels
% 
We will now report on the lessons learned in a time line-based manner. Depending on the reader's background, either in computer science or the humanities for instance, some of the reported obstacles may appear 'trivial'. However, we claim that it is crucial to raise awareness for false assumptions, made by domain experts, e.g., with regard to common knowledge about specific technological or methodological aspects of an experiment, which are by far not so obvious/common for others --- outside their domain. Furthermore, we would like to point out that the reported observations are based on a \emph{practical interdisciplinary replication attempt}, which is especially valuable in order to learn about all \emph{the different characteristics} and challenges concerning replicability of robotics systems.

The replication attempt of this JSE experiment was conducted by a research assistant (RA) with a background in psychology. With respect to interdisciplinary research this was, on the one hand, an almost ideal scenario, on the other hand however, a technically-challenging one as well.

\subsection{Technical Obstacles \& Procedural Issues}

The following issues were reported during the replication study in Indiana. 
The research assistant started with a plain laptop. Thus, the first issue was reported shortly after the study officially started. Even though the deployment of the required software components (using the CITK) was successful on first attempt, the RA faced a couple of issues with the installation routine of
Ubuntu. The team in Bielefeld could resolve these issues by pointing the RA to the correct Ubuntu documentation.

The second technical issue was reported a few days later. The operating system, as well as the robot software environment, were already installed successfully. Nonetheless, during the required robot calibration procedure, a connection to the robot could not be established via local network. 
The team in Bielefeld resolved this issue by instantly updating the online manual for the network setup which is
also hosted in the linked repositories.

The third issue was reported after a first test run of the experiment. So far, the entire software system was deployed, the robot calibrated, and also the physical experiment setup was in place. However, during a the test run, the RA  noticed that the translation of two single lines of text on a slide in the jsPsych time line was missing. The team in Bielefeld could resolve this issue by correcting the error in the code base and subsequently updating the git repository. In Indiana, the RA just had to re-trigger the corresponding build job, thus automatically installing the updated version of the experiment.

The fourth, and last reported obstacle occurred in an early stage of the actual experiment. Since the tool chain allows to download and inspect already gathered data via web browser, the colleagues in Indiana soon took a first look at intermediate results. They noticed that the distribution of the participants position with respect to the robot indicated a strong preference towards only \emph{one} side. The team in Bielefeld discovered that the instructions provided for the experimenter in jsPsych, addressing the procedure of subject positioning, were not as precise as they should have been.
This could be corrected by updating the description in the repository.

\subsection{Results of the Pilot Study on Reproducibility in HRI}

At time of writing, the JSE experiment in Indiana has been finished --- first results show a 
weaker but observable Joint Simon effect. However, besides the obstacles already discussed there
were several positive lessons learned.

It is very difficult, if not impossible, to foresee all pitfalls faced by the researcher replicating the experiment.
As a local solution or patch does not solve the issue in a consistent manner, a flexible tool chain is required, that allows for almost instant patching and deployment of experiment artifacts, fosters the reproduction process as presented in this context. In this regard, the technical complexity of the deployed robotic system (hard- and software) was completely hidden to the research assistant (RA) in Indiana and was of \emph{no consequence at all}. 

Also, the time required to setup the entire software system was limited to a few hours, including the installation of an operating system. Moreover, the acceptance threshold and usability of the CITK tool chain appeared to be acceptable, given the fact that it was easily usable by the RA. Also, the transition from design, implementation and execution of the experiment in Bielefeld, to the deployment in Indiana was merely sending a link to an online manual.

In a short post-experiment interview we asked the RA for a self-assessment regarding the experience with Linux-based systems, robotic hardware, robotic software, the Linux network stack, conducting HRI experiments, and conducting psychology experiments. In summation, the RA was reasonably experienced in conducting experiments in psychology and knew a few basic Linux commands, thus had used Linux before. Regarding the remaining topics, the RA was an inexperienced user, i.e, had never operated a single robot before.

\section{Summary and Recommendations}
% 1 page
% discuss lessons learned - what went well/wrong, easy/hard
% suggest recommendations and perspectives for future work
%
In the final section of this contribution we will now discuss the lessons learned and correlate them with the general debate in robotics concerning reproducibility.

\paragraph{Reproducibility is decided at development time} We would like to stress that without
having the tool chain in place at the development and preparation time of the study in Bielefeld,
a replication study at Indiana would have been extremely time consuming if not impossible.
Thus, any tool chain used for the replication of results should be established in the daily workflow
of the researchers understanding it as a \emph{development tool} instead of a replication tool.  

\paragraph{Experimental protocols} Besides the technical requirements and issues to replicate a study
and their scientific results, it is also important to neatly document the experimental protocol.
Typically, this is solved by workflows, policies, and tools within the specific discipline without
being integrated in the technical framework of a robotic experiment.
In the study reported, a tool from psychology was integrated for experimental control. This is also a prerequisite for a systematic logging of all experimental data. However, we can observe in the study that the
non-technical aspects of the experimental protocol were not sufficiently described which raised
several questions when intermediate results were analysed and discussed. Thus, there is still
an open issue to more formally describe the experimental protocols.

\paragraph{Scientists with a non-technical background} An interdisciplinary field like HRI involves
experts from different backgrounds. Reproducibility should not depend on having a robotic expert
on-site. Even though the current approach demonstrated that \emph{it is possible}, even by an inexperienced user, these first obstacles where not even close to what a robotics engineer would consider ``an obstacle''. On this account, we deem this lesson learned even more valuable. Furthermore, these kind of ``low-level'' obstacles can be easily mitigated by providing detailed \emph{beginner-level instructions}. 

\paragraph{Automated documentation roll-out}
It appears extremely useful to be able to quickly and dynamically alter instructions provided for replication attempts if errors are reported/discovered. Also, SCM-based repositories, not only for source code, but also for these kind of manuals seem to be a well-applicable solution. Additionally, adding replication instructions to the corresponding source code of a publication is not labor intensive at all. 

\paragraph{Report intermediate results} The issues and obstacles discussed before imply that it is important to automate the collection and evaluation of (also) intermediate results to prevent subsequent failures, especially if data acquisition is time-consuming. Thus, the requirement for an analytic reproducibility also applies to intermediate results. In the case of the experiment considered here, all preprocessing steps and scripts were precisely documented. Further,  the 'R'-toolbox
as an alternative for SPSS for generating final detailed statistics is open source.

\paragraph{Open issues} Regarding the limitations of the approach presented, the toolbox currently does not incorporate any standardized benchmark procedures for more general HRI experiments. It does not provide any tool or template support for metrics (if existent/agreed-upon) with respect to comparability of HRI systems. We are open for discussion and welcome contributions concerning this topic.

\paragraph{Final remarks:} In this contribution we discussed and analyzed a dedicated study on the replication of a reasonably complex HRI experiment in two different laboratories across the globe without a) flying experts in and b) making a single video/phone call --- by a research assistant with \emph{a non-technical background} and no experience in robotics at all. We reported on the lessons learned during this practical replication process. 



%% The Appendices part is started with the command \appendix;
%% appendix sections are then done as normal sections
%% \appendix

%% \section{}
%% \label{}

%% References
%%
%% Following citation commands can be used in the body text:
%% Usage of \cite is as follows:
%%   \cite{key}          ==>>  [#]
%%   \cite[chap. 2]{key} ==>>  [#, chap. 2]
%%   \citet{key}         ==>>  Author [#]

%% References with bibTeX database:

\bibliographystyle{model1-num-names}
\bibliography{bib9ch}

%% Authors are advised to submit their bibtex database files. They are
%% requested to list a bibtex style file in the manuscript if they do
%% not want to use model1-num-names.bst.

%% References without bibTeX database:

% \begin{thebibliography}{00}

%% \bibitem must have the following form:
%%   \bibitem{key}...
%%

% \bibitem{}

% \end{thebibliography}


\end{document}

%%
%% End of file `elsarticle-template-1-num.tex'.